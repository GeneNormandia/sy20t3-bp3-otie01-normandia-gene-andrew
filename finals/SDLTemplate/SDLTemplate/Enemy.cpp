#include "Enemy.h"
#include "Scene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{

}

void Enemy::start()
{
	//Load Texture
	texture = loadTexture("gfx/enemy.png");

	//Initialize to avoid garbage
	timer = 0;
	count = 1;
	directionX = 1;
	directionY = -1;
	width = 0;
	height = 0;
	speed = 2;
	reloadTime = 40;//Reload time of 8 frames, or 1 seconds
	currentReloadTime = 0;
	//directionChangeTime = (rand() % 300) + 100;//Direction change time for 3-8 seconds
	directionChangeTime = (rand() % 60) + 60;
	currentDirectionChangeTime = 0;

	//Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 50;
}

void Enemy::update()
{
		spawn();

}

void Enemy::draw()
{
	blit(texture, x, y);
}

void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Enemy::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

void Enemy::setToBoss()
{
	directionY = 0;
	reloadTime = 10;
	count++;

}

int Enemy::getPositionX()
{
	return x;
}

int Enemy::getPositionY()
{
	return y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}

void Enemy::spawn()
{
	x += directionX * speed;
	y -= directionY * speed;

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;
	if (currentDirectionChangeTime == 0)
	{
		directionX = -directionX;
		//directionY = -directionY;
		currentDirectionChangeTime = directionChangeTime;
	}

	//Decrement the enmy's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;
	if (currentReloadTime == 0)
	{
		float dx = -1;
		float dy = 0;

		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);
		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x + width, y - 2 + height / 2, dx, dy, 8, Side::ENEMY_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);
		//After firing, reset our reload timer
		currentReloadTime = reloadTime;
	}
	//Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		//(bullets[i]->getPositionY() > 720)
		if (bullets[i]->getPositionY() > 720)
		{
			//Cache the variable so we can delete it later 
			//We can't delete it after erasing from the vector(leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			//Delete only 1 bullet per frame
			//delete this;
			break;
		}
	}
	if (y > 720)
	{
		delete this;
	}
}
