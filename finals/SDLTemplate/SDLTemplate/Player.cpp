#include "Player.h"
#include "Scene.h"
Player::~Player()
{
	//Memory manage our bullets. Delete all bullets on player deletion/death
	for (int i = 0; i < bullets.size(); i++)
	{
		delete bullets[i];
	}
	bullets.clear();
}
void Player::start()
{

	//Load Texture
	texture = loadTexture("gfx/player.png");

	//Initialize to avoid garbage
	firePattern = 0;
	x = 220;
	y = 600;
	width = 0;
	height = 0;
	speed = 5;
	reloadTime = 20;//Reload time of 8 frames, or 0.13 seconds
	currentReloadTime = 0;
	isAlive = true;

	//Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Player::update()
{
	//Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionY() < 0)
		{
			//Cache the variable so we can delete it later 
			//We can't delete it after erasing from the vector(leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			//Delete only 1 bullet per frame
			break;
		}
	}

	if (!isAlive) return;

	if (app.keyboard[SDL_SCANCODE_W])
	{
		y -= speed;

	}
	if (app.keyboard[SDL_SCANCODE_S])
	{
		y += speed;
	}
	if (app.keyboard[SDL_SCANCODE_A])
	{
		x -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_D])
	{
		x += speed;
	}

	//Decrement the player's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;
	if (app.keyboard[SDL_SCANCODE_F] && currentReloadTime == 0)
	{
		SoundManager::playSound(sound);
		if (firePattern == 0)
		{
			Bullet* bullet = new Bullet(x - 5 + height / 2, y, 0, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);
		}
		else if (firePattern == 1)
		{
			reloadTime = 10;
			Bullet* bullet = new Bullet(x - 10 + height / 2, y, 0, -1, 10, Side::PLAYER_SIDE);
			Bullet* bullet1 = new Bullet(x - 1 + height / 2, y, 0, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			bullets.push_back(bullet1);
			getScene()->addGameObject(bullet);
			getScene()->addGameObject(bullet1);
		}
		else if (firePattern >= 2)
		{
			reloadTime = 10;
			Bullet* bullet = new Bullet(x - 5 + height / 2, y, -.1, -1, 10, Side::PLAYER_SIDE);
			Bullet* bullet1 = new Bullet(x - 5 + height / 2, y, 0, -1, 10, Side::PLAYER_SIDE);
			Bullet* bullet2 = new Bullet(x - 5 + height / 2, y, .1, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			bullets.push_back(bullet1);
			bullets.push_back(bullet2);
			getScene()->addGameObject(bullet);
			getScene()->addGameObject(bullet1);
			getScene()->addGameObject(bullet2);
		}
		


		//After firing, reset our reload timer
		currentReloadTime = reloadTime;
	}
}

void Player::draw()
{
	if (!isAlive) return;

	blit(texture, x, y);
}

int Player::getPositionX()
{
	return x;
}

int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height
;
}

bool Player::getIsAlive()
{
	return isAlive;
}

void Player::doDeath()
{
	isAlive = false; 
}

//Modify the firerate of the player when a power up is picked up
void Player::powerUp()
{
	/*if (reloadTime > 5)
	{
		reloadTime -= 3;
	}
	else
	{
		return;
	}*/
	firePattern++;
}
