#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Boss.h"
#include <vector>
#include "text.h"
class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;
	Enemy* enemy;
	Boss* boss;

	//Enemy spawn logic
	float spawnTime;
	float currentSpawnTimer;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<Boss*> spawnedBoss;
	void doSpawnLogic();
	void doCollisionLogic();
	void spawn();
	void spawnBoss();
	void despawnEnemy(Enemy* enemy);
	void despawnPowerUp(Bullet* bullet);
	void spawnPowerUP();
	int wave;
	int powerUpTimer;
	int powerTime;
	int bossCount;
};

