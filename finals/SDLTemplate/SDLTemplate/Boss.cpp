#include "Boss.h"
#include "Scene.h"


Boss::Boss()
{
}

Boss::~Boss()
{
}

void Boss::start()
{
	initFonts();
	hp = 1000;
	//Load Texture
	texture = loadTexture("gfx/enemy.png");

	//Initialize to avoid garbage
	timer = 0; //change the fire rate over time
	directionX = 1;
	directionY = -1;
	width = 0;
	height = 0;
	speed = 3;
	reloadTime = 10;//Reload time of 8 frames, or 1 seconds
	currentReloadTime = 0;
	//directionChangeTime = (rand() % 60) + 60;
	directionChangeTime = 100;
	currentDirectionChangeTime = 0;

	//Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 50;
}

void Boss::update()
{
	std::cout << "Timer: " << timer << std::endl;
	if (hp <= 0)
		return;
	x += directionX * speed;

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;
	if (currentDirectionChangeTime == 0)
	{
		directionX = -directionX;
		currentDirectionChangeTime = directionChangeTime;
	}
	

	//Decrement the enemy's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;
	if (currentReloadTime == 0)
	{
		reloadTime = 10;
		float dx = -1;
		float dy = 0;

		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);
		SoundManager::playSound(sound);

		float bx = -.8;
		if (timer < 180)
		{
			Bullet* bullet = new Bullet(x + width/ 2, y - 2 + height / 2, 0, 1, 10, Side::ENEMY_SIDE);
			Bullet* bullet2 = new Bullet(x + width/ 4, y - 2 + height / 2, 0, 1, 10, Side::ENEMY_SIDE);
			bullets.push_back(bullet);
			bullets.push_back(bullet2);
			getScene()->addGameObject(bullet);
			getScene()->addGameObject(bullet2);
		}
		else //change the fire rate of the boss
		{
			for (int i = 0; i < 8; i++)
			{
				Bullet* bullet = new Bullet(x + width/2, y - 2 + height / 2, bx, 1, 6, Side::ENEMY_SIDE);
				bullets.push_back(bullet);
				getScene()->addGameObject(bullet);
				bx += .2;
			}
		}
		if (timer > 360)
			timer = 0;
		currentReloadTime = reloadTime;
	}

	//Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionY() > 720)
		{
			//Cache the variable so we can delete it later 
			//We can't delete it after erasing from the vector(leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			//Delete only 1 bullet per frame
			//delete this;
			break;
		}
	}
	timer++;
}


void Boss::draw()
{
	if (hp <= 0)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
	if (hp <= 0)
		return;;
	blit(texture, x, y);

}

void Boss::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Boss::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Boss::getPositionX()
{
	return x;
}

int Boss::getPositionY()
{
	return y;
}

int Boss::getWidth()
{
	return width;
}

int Boss::getHeight()
{
	return height;
}

void Boss::spawn()
{
	
}

void Boss::clearBullets()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		//(bullets[i]->getPositionY() > 720)
		if (bullets[i]->getPositionY() > 500)
		{
			//Cache the variable so we can delete it later 
			//We can't delete it after erasing from the vector(leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			//Delete only 1 bullet per frame
			//delete this;
			break;
		}
	}
}

void Boss::bossTakeDamage()
{
	hp--;
}


