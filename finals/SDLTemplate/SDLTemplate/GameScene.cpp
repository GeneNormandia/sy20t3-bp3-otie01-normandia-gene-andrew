#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	wave = 1;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here
	initFonts();
	powerUpTimer = 600;
	currentSpawnTimer = 120;
	powerTime = 1000;
	spawnTime = 300;//Spawn time of 5 seconds
	bossCount = 0;

	for (int i = 0; i < 5; i++)
	{
		Enemy* enemy = new Enemy();
		this->addGameObject(enemy);
		enemy->setPlayerTarget(player);

		enemy->setPosition(150 + (rand() % 400), 100);
		spawnedEnemies.push_back(enemy);
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "WAVE: %01d", wave);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();
	if (wave < 10)
	{
		doSpawnLogic();
		spawnPowerUP();
	}
	else if (wave >= 10)
	{
		spawnBoss();
	}
	doCollisionLogic();
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;
	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 7; i++)
		{
			spawn();
		}
		currentSpawnTimer = spawnTime;
		wave++;
	}
}

void GameScene::doCollisionLogic()
{
	//Check for collision
	for (int i = 0; i < objects.size(); i++)
	{
		//Cast to bullet
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		//Check if the cast was successful (i.e. objects[i] is a Bullet)
		if (bullet != NULL)
		{
			//If the bullet is from the enemy side, check against the player
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			//If the bullet is from the player side, check against ALL enemies
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];
					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);
					if (collision == 1)
					{
						despawnEnemy(currentEnemy);
						break;
					}

				}
			}
			//If player picks up a power up
			else if (bullet->getSide() == Side::POWER_UP)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->powerUp();
					despawnPowerUp(bullet);
					break;
				}
			}
			//If the boss is hit
			if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedBoss.size(); i++)
				{
					Boss* boss = spawnedBoss[i];
					int collision = checkCollision(
						boss->getPositionX(), boss->getPositionY(), boss->getWidth(), boss->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);
					if (collision == 1)
					{
						boss->bossTakeDamage();
						break;
					}

				}
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);
	enemy->setPosition(100 + (rand() % 200), 100);
	spawnedEnemies.push_back(enemy);
}

void GameScene::spawnBoss()
{
	if (bossCount == 0)
	{
		Boss* boss = new Boss();
		this->addGameObject(boss);
		boss->setPlayerTarget(player);
		boss->setPosition(400, 0);
		//boss->setPosition(400, 0); //original boss position
		spawnedBoss.push_back(boss);
		bossCount++;
	}
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}
	if (index != 1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::despawnPowerUp(Bullet* bullet)
{
		delete bullet;
}

void GameScene::spawnPowerUP()
{
	if (powerUpTimer > 0)
		powerUpTimer--;
	if (powerUpTimer <= 0)
	{
		Bullet* bullet = new Bullet(50+ (rand()% 300), 200, 0, 1, 1, Side::POWER_UP);
		this->addGameObject(bullet);
		powerUpTimer = powerTime;
	}
}



